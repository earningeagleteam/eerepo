package com.farmcart.client;

import com.farmcart.server.UserServiceImpl;
import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import com.google.web.bindery.requestfactory.shared.Service;


@Service( value = UserServiceImpl.class )
public interface SaveUserRequestContext  extends RequestContext{
	Request<String> saveUserDetails(String userName);

}
