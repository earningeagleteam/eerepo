package com.farmcart.client;

import com.farmcart.client.login.LoginPageView;
import com.farmcart.client.shopkeeper.shopkeeperAfterLogin;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.web.bindery.requestfactory.shared.Receiver;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Egle implements EntryPoint {

	private final GreetingServiceAsync greetingService = GWT
			.create(GreetingService.class);

	public void onModuleLoad() {

		final LoginPageView loginView = new LoginPageView();
        final shopkeeperAfterLogin sal = new shopkeeperAfterLogin();
		
        RootPanel.get("nameFieldContainer").add(sal);

		// Create a handler for the sendButton and nameField
		class MyHandler implements ClickHandler, KeyUpHandler {
			/**
			 * Fired when the user clicks on the sendButton.
			 */
			public void onClick(ClickEvent event) {

				String userName = loginView.getUsername().getValue();
				RequestFactoryUtil.getRequestFactory()
						.getSaveUserRequestContext().saveUserDetails(userName)
						.fire(new Receiver<String>() {

							@Override
							public void onSuccess(String result) {

								PopupPanel dailog = new PopupPanel();
								Label userName = new Label(result);
								HorizontalPanel welcome = new HorizontalPanel();
								welcome.add(userName);
								dailog.add(welcome);
								dailog.center();
								dailog.show();
							}
						});
				// sendNameToServer();
			}

			/**
			 * Fired when the user types in the nameField.
			 */
			public void onKeyUp(KeyUpEvent event) {

			}

		}

		// Add a handler to send the name to the server
		MyHandler handler = new MyHandler();
		loginView.getLogin().addClickHandler(handler);

	}
}
