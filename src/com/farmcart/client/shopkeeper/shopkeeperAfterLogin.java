package com.farmcart.client.shopkeeper;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;

public class shopkeeperAfterLogin extends Composite {

	private static shopkeeperAfterLoginUiBinder uiBinder = GWT
			.create(shopkeeperAfterLoginUiBinder.class);

	interface shopkeeperAfterLoginUiBinder extends
			UiBinder<Widget, shopkeeperAfterLogin> {
	}

	public shopkeeperAfterLogin() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	public shopkeeperAfterLogin(String firstName) {
		initWidget(uiBinder.createAndBindUi(this));

	}

	@UiHandler("btnAddProduct")
	void onBtnAddProductClick(ClickEvent event) {
		final AddProductUI apu = new AddProductUI();
		RootPanel.get("nameFieldContainer").clear();
		RootPanel.get("nameFieldContainer").add(apu);
	}
}
