package com.farmcart.client.login;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class LoginPageView extends Composite {

	private static LoginPageViewUiBinder uiBinder = GWT
			.create(LoginPageViewUiBinder.class);

	interface LoginPageViewUiBinder extends UiBinder<Widget, LoginPageView> {
	}

	@UiField
	TextBox username;
	@UiField
	TextBox password;
	@UiField
	Button login;

	public LoginPageView() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	public LoginPageView(String firstName) {
		initWidget(uiBinder.createAndBindUi(this));
	}

	public TextBox getUsername() {
		return username;
	}

	public TextBox getPassword() {
		return password;
	}

	public Button getLogin() {
		return login;
	}

}
