package com.farmcart.client;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.SimpleEventBus;
import com.google.web.bindery.requestfactory.gwt.client.DefaultRequestTransport;

public class RequestFactoryUtil {
	
	private static ApplicationRequestFactory requestFactory = GWT.create( ApplicationRequestFactory.class );

	private static EventBus eventBus = new SimpleEventBus();

	private static DefaultRequestTransport transport = new DefaultRequestTransport();

	static {
		getRequestFactory().initialize( eventBus, transport );
	}

	public static ApplicationRequestFactory getRequestFactory() {
		return requestFactory;
	}

}
