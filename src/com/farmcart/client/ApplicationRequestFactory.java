package com.farmcart.client;

import com.google.web.bindery.requestfactory.shared.RequestFactory;

public interface ApplicationRequestFactory extends RequestFactory{
	
	SaveUserRequestContext getSaveUserRequestContext();


}
