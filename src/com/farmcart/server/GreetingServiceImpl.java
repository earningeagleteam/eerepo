package com.farmcart.server;

import com.farmcart.client.GreetingService;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class GreetingServiceImpl extends RemoteServiceServlet implements
		GreetingService {

	public String greetServer(String input) throws IllegalArgumentException {
	
		System.out.println("Entered User is::"+input);

		return "Hello" + input+"Successfully logged";
	}
	
	
	
	
}
